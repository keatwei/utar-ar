using UnityEngine;
using System.Collections;

public class VirtualButtonEventHandle : MonoBehaviour , IVirtualButtonEventHandler{
	int sound = 0;
	
	int about = 0;
	
	int sunlight = 1;
	
	private GameObject text;
	private GameObject aboutPlane;
	private GameObject sunny;
	private GameObject point1;
	private GameObject point2;
	private GameObject point3;
	private GameObject point4;
	
	// Use this for initialization
	void Start () {
		VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour> ();
		for(int i=0; i < vbs.Length; i++){
			vbs[i].RegisterEventHandler(this);
		}
		
		text = transform.FindChild("text").gameObject;
		aboutPlane = transform.FindChild("AboutPlane").gameObject;
		sunny = transform.FindChild("SunLight").gameObject;
		point1 = transform.FindChild("PointLight1").gameObject;
		point2 = transform.FindChild("PointLight2").gameObject;
		point3 = transform.FindChild("PointLight3").gameObject;
		point4 = transform.FindChild("PointLight4").gameObject;
		sunny.SetActive(true);
		point1.SetActive(false);
		point2.SetActive(false);
		point3.SetActive(false);
		point4.SetActive(false);
		text.SetActive(false);
		aboutPlane.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnButtonPressed(VirtualButtonAbstractBehaviour vb){
		
		if(vb.VirtualButtonName == "AboutButton"){
				
			if(about == 0){
				text.SetActive(true);
				aboutPlane.SetActive(true);
				about = 1;
			}else if(about == 1){
				text.SetActive(false);
				aboutPlane.SetActive(false);
				about = 0;
			}
			
		}
		
		if(vb.VirtualButtonName == "SoundButton"){
			if(sound == 0){
				audio.Play ();
				sound = 1;
			}else if(sound == 1){
				audio.Stop ();
				sound = 0;
			}
		}
		
		if(vb.VirtualButtonName == "SunlightButton"){
			if(sunlight == 1){
				sunny.SetActive(false);
				sunlight = 0;
				point1.SetActive(true);
				point2.SetActive(true);
				point3.SetActive(true);
				point4.SetActive(true);
				Debug.Log (sunlight);
			}else if(sunlight == 0){
				sunny.SetActive(true);
				sunlight = 1;
				point1.SetActive(false);
				point2.SetActive(false);
				point3.SetActive(false);
				point4.SetActive(false);
		
				
			}
		}
		
	}
	
	public void OnButtonReleased(VirtualButtonAbstractBehaviour vb){
	}
}
